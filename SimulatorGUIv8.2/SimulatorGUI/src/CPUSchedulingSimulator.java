import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;



public class CPUSchedulingSimulator {
    static List<Process> processes = new ArrayList<>();
    static AtomicInteger completedProcesses = new AtomicInteger(0);
    static final int cpus = 4;
    static Timer timer = new Timer();
    static TimerTask timerTask;

    static JPanel panel = MainFrame.simulatorPanel;
    static int RRxlocation = 230;
    static int FCFSxlocation = 230;
    static int NPPSxlocation = 230;
    static int SJFxlocation = 230;
    public static volatile boolean simulationRunning = true;
    static double turnSum;
    static double waitSum;
    static AtomicBoolean isPaused = new AtomicBoolean(false);
    static List<Processor> processorsList = new ArrayList<>();
    static List<Thread> threadList = new ArrayList<>();

    public static void startSimulation(JPanel simPanel) {
        panel = simPanel;

        timerTask = new TimerTask() {
            @Override
            public void run() {
                SwingUtilities.invokeLater(() -> {
                    MainFrame.timerLabel.setText("Timer: " + SystemTimer.getCurrentTime());
                    System.out.println("Current time: " + SystemTimer.getCurrentTime());
                    updateTable();
                });

                if (completedProcesses.get() >= processes.size()) {
                    simulationRunning = false; // Stop the simulation
                    timer.cancel();
                    System.out.println("All processes have been completed. Ending simulation...");

                    SwingUtilities.invokeLater(() -> {
                        JOptionPane.showMessageDialog(null, "Simulation Complete", "Simulation Finished", JOptionPane.INFORMATION_MESSAGE);
                        double avgTurn = turnSum / processes.size();
                        double avgWait = waitSum / processes.size();
                        MainFrame.avgTurn.setText("" + avgTurn);
                        MainFrame.avgWait.setText("" + avgWait);
                    });
                    stopSimulation();

                }

                if (!isPaused.get() && simulationRunning) {
                    SystemTimer.incrementTime(1);
                }

                // Classify processes into categories
                Map<String, List<Process>> classifiedProcesses = new HashMap<>();
                for (Process process : processes) {
                    classifiedProcesses
                            .computeIfAbsent(process.classification, k -> new ArrayList<>())
                            .add(process);
                }

                // Process each classification
                for (String classification : Arrays.asList("Realtime", "System", "Interactive", "Batch")) {
                    List<Process> classifiedQueue = classifiedProcesses.get(classification);
                    if (classifiedQueue != null) {
                        sortProcessesByClassification(classifiedQueue, classification);

                        for (Process process : classifiedQueue) {
                            if (process.arrivalTime == SystemTimer.getCurrentTime() && process.remainingTime >= 0 && !isPaused.get()) {

                                    if (!QueueManager.mainQueue.contains(process)) {
                                        QueueManager.mainQueue.add(process);
                                        System.out.println("Process " + process.pid + " added to " + classification + " queue at time " + SystemTimer.getCurrentTime());
                                        if(QueueManager.mainQueue.contains(process)){
                                        addAnimationManager(process, classification.equals("Realtime") ? "RR" : classification.equals("System") ? "FCFS" : classification.equals("Interactive") ? "NPPS" : "SJF");}
                                    }

                            }
                        }
                    }
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);

        // Start processor threads
        for (int i = 1; i <= cpus; i++) {
            Processor processor = new Processor(i, panel);
            Thread processorThread = new Thread(processor);
            processorsList.add(processor);
            threadList.add(processorThread);
            processorThread.start();
        }
    }

    private static void sortProcessesByClassification(List<Process> processes, String classification) {
        switch (classification) {
            case "Realtime":
            case "System":
                processes.sort(Comparator.comparingInt(Process::getId));
                break;
            case "Interactive":
                processes.sort(Comparator.comparingInt(Process::getPriority).thenComparingInt(Process::getId));
                break;
            case "Batch":
                processes.sort(Comparator.comparingInt(Process::getBurstTime).thenComparingInt(Process::getId));
                break;
        }
    }

    public static synchronized void completeProcess(Process process) {
        process.turnaroundTime = SystemTimer.getCurrentTime() - process.arrivalTime;
        process.waitingTime = process.turnaroundTime - process.burstTime;
        process.terminatedTime = SystemTimer.getCurrentTime();
        process.status = "Terminated";
        turnSum += process.turnaroundTime;
        waitSum += process.waitingTime;
        completedProcesses.incrementAndGet();

        System.out.println("Process " + process.pid + " completed at time " + process.terminatedTime);
    }

    public static void addAnimationManager(Process process, String schedulingType) {
        SwingUtilities.invokeLater(() -> {
            JLabel animationLabel;
            switch (schedulingType) {
                case "RR":
                    animationLabel = new RealTimeAnimationManager(RRxlocation, process.pid);
                    RRxlocation -= 30;
                    break;
                case "FCFS":
                    animationLabel = new FCFSAnimationManager(FCFSxlocation, process.pid);
                    FCFSxlocation -= 30;
                    break;
                case "NPPS":
                    animationLabel = new InteractiveAnimationManager(NPPSxlocation, process.pid);
                    NPPSxlocation -= 30;
                    break;
                case "SJF":
                    animationLabel = new BatchAnimationManager(SJFxlocation, process.pid);
                    SJFxlocation -= 30;
                    break;
                default:
                    return;
            }
            panel.add(animationLabel);
            panel.repaint();
            animationLabel.setVisible(true);
        });
    }

    public static void updateProcess(Process updatedProcess) {
        synchronized (processes) {
            for (int i = 0; i < processes.size(); i++) {
                if (processes.get(i).pid == updatedProcess.pid) {
                    processes.set(i, updatedProcess);
                    break;
                }
            }
        }
    }

    private static void updateTable() {
        SwingUtilities.invokeLater(() -> MainFrame.updateProcessTable(CPUSchedulingSimulator.processes));
    }

    public static void pauseSimulation() {
        isPaused.set(true);
    }

    public static void resumeSimulation() {
        isPaused.set(false);
        synchronized (isPaused) {
            isPaused.notifyAll();
        }
    }

    public static void stopSimulation() {
        if (timerTask != null) {
            timerTask.cancel();
        }
        timer.cancel();
        timer.purge();
        isPaused.set(false);

        // Stop all processor threads
        for (Processor processor : processorsList) {
            processor.stopProcessing();
        }
    }

    public static void interruptProcessorThreads() {
        for (Thread processorThread : threadList) {
            processorThread.interrupt(); // Interrupt each processor thread
        }
    }

    public static void reset() {
        clearAnimations();
        panel.removeAll();
        panel.repaint();
        // Stop the current timer and task
        completedProcesses.set(0);
        SystemTimer.reset();
        if (timerTask != null) {
            timerTask.cancel();
        }
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        // Stop all processor threads
        for (Processor processor : processorsList) {
            processor.stopProcessing();
        }
        processorsList.clear();

        interruptProcessorThreads();

        // Clear main queue
        QueueManager.mainQueue.clear();

        // Clear all processes
        synchronized (processes) {
            processes.clear();
        }
            // Reset GUI components and locations
        RRxlocation = 230;
        FCFSxlocation = 230;
        NPPSxlocation = 230;
        SJFxlocation = 230;
        turnSum = 0;
        waitSum = 0;
        MainFrame.timerLabel.setText("Timer: 0");
        MainFrame.avgTurn.setText("Average Turnaround Time: 0");
        MainFrame.avgWait.setText("Average Waiting Time: 0");

        timer = new Timer();
        timerTask = null;
    }

    public static void clearAnimations() {
        SwingUtilities.invokeLater(() -> {
            Component[] components = panel.getComponents();
            for (Component component : components) {
                if (component instanceof JLabel) {
                    panel.remove(component);
                }
            }
            panel.repaint();
        });

        // Reset the locations for the animations
        RRxlocation = 230;
        FCFSxlocation = 230;
        NPPSxlocation = 230;
        SJFxlocation = 230;
    }
}
