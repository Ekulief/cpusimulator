import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;


public class GanttChartPanel extends JPanel {
    private ArrayList<Process> processes;
    private ArrayList<Color> processColors; // Store colors for each process
    private final Random random = new Random();

    public GanttChartPanel(ArrayList<Process> processes) {
        this.processes = processes;
        this.processColors = generateRandomColors(processes.size());
        setLayout(new BorderLayout());

        JPanel panel = new JPanel() {
            private final int RECT_HEIGHT = 40;
            private final int MAX_WIDTH = 350; // Maximum width constraint for the chart

            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                int currentTime = 0;
                int totalBurstTime = getTotalBurstTime();

                for (int i = 0; i < processes.size(); i++) {
                    Process process = processes.get(i);
                    Color color = processColors.get(i); // Get color from the pre-generated list
                    int width = calculateWidth(process.getBurstTime(), totalBurstTime);
                    int burstTime = process.getBurstTime();



                    if (currentTime < process.getArrivalTime()) {
                        int idleTime = process.getArrivalTime() - currentTime;
                        g.setColor(Color.gray);
                        int idleWidth = calculateWidth(idleTime, totalBurstTime);
                        g.fillRect(currentTime * MAX_WIDTH / totalBurstTime, 0, idleWidth, RECT_HEIGHT);
                        currentTime += idleTime;
                    }

                    g.setColor(color);
                    g.fillRect(currentTime * MAX_WIDTH / totalBurstTime, 0, width, RECT_HEIGHT);

                    // Draw process name at the center of each rectangle
                    g.setColor(Color.BLACK);
                    FontMetrics metrics = g.getFontMetrics();
                    int textWidth = metrics.stringWidth(Integer.toString(process.getId()));
                    int textX = currentTime * MAX_WIDTH / totalBurstTime + (width - textWidth) / 2;
                    int textY = (RECT_HEIGHT - metrics.getHeight()) / 2 + metrics.getAscent();
                    g.drawString("P" + Integer.toString(process.getId()), textX, textY);

                    // Display start and end times at the edges of each rectangle
                    g.drawString(String.valueOf(currentTime), currentTime * MAX_WIDTH / totalBurstTime, RECT_HEIGHT + 15);
                    g.drawString(String.valueOf(currentTime + process.getBurstTime()), (currentTime + process.getBurstTime()) * MAX_WIDTH / totalBurstTime, RECT_HEIGHT + 15);

                    currentTime += process.getBurstTime();
                }
            }

            @Override
            public Dimension getPreferredSize() {
                int totalBurstTime = getTotalBurstTime();
                int width = totalBurstTime * MAX_WIDTH / totalBurstTime;
                return new Dimension(Math.max(width, MAX_WIDTH), RECT_HEIGHT + 40); // Adjusted height to fit text and rectangles
            }

            private int getTotalBurstTime() {
                return processes.stream().mapToInt(Process::getBurstTime).sum();
            }

            private int calculateWidth(int burstTime, int totalBurstTime) {
                return burstTime * MAX_WIDTH / totalBurstTime;
            }
        };

        panel.setLayout(new FlowLayout(FlowLayout.LEFT)); // Align content to the top left
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(new Dimension(600, 70)); // Adjusted scroll pane size
        add(scrollPane, BorderLayout.CENTER);
    }

    private ArrayList<Color> generateRandomColors(int count) {
        ArrayList<Color> colors = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            float hue = random.nextFloat();
            float saturation = 0.6f; // 0.6 for vibrant colors
            float brightness = 0.9f; // 0.9 for bright colors
            Color color = Color.getHSBColor(hue, saturation, brightness);
            colors.add(color);
        }
        return colors;
    }
}