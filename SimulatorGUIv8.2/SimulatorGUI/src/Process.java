public class Process {
    public int pid;
    public int arrivalTime;
    public int burstTime;
    public int remainingTime;
    public int priority;
    public  String classification;
    public int turnaroundTime;
    public  int waitingTime;
    public int terminatedTime;

    public String status;

    public Process(int pid, int arrivalTime, int burstTime, int priority, String classification ,String status) {
        this.pid = pid;
        this.arrivalTime = arrivalTime;
        this.burstTime = burstTime;
        this.remainingTime = burstTime;
        this.priority = priority;
        this.classification = classification;
        this.status = status;
    }


    // Getters and Setters for id
    public int getId() {
        return pid;
    }

    public void setId(int id) {
        this.pid = id;
    }

    // Getters and Setters for arrivalTime
    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    // Getters and Setters for burstTime
    public int getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(int burstTime) {
        this.burstTime = burstTime;
    }

    // Getters and Setters for priority
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    // Getters and Setters for classification
    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTurnaroundTime(int turnaroundTime) {
        this.turnaroundTime = turnaroundTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getTurnaroundTime() {
        return turnaroundTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public int getRemainingTime() {
        return remainingTime;
    }

    public  int preferredCPU() {
        switch (classification) {
            case "Realtime":
                return 1;
            case "System":
                return 2;
            case "Interactive":
                return 3;
            case "Batch":
                return 4;
            default:
                return 0;
        }
    }

}
