class ProcessExecutionLog {
    int cpuId;
    int processId;
    int startTime;
    int endTime;
    String schedulingType;

    public ProcessExecutionLog(int cpuId, int processId, int startTime, int endTime, String schedulingType) {
        this.cpuId = cpuId;
        this.processId = processId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.schedulingType = schedulingType;
    }
}
