import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class ProcessTableModel extends AbstractTableModel {
    private List<Process> processes;
    private final String[] columnNames = {"PID", "Status", "Burst Time", "Waiting Time", "Turnaround Time"};

    public ProcessTableModel() {
        this.processes = new ArrayList<>();
    }

    public void setProcesses(List<Process> processes) {
        this.processes = processes;
    }

    @Override
    public int getRowCount() {
        return processes.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Process process = processes.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return process.pid;
            case 1:
                return process.getStatus();
            case 2:
                return process.remainingTime;
            case 3:
                return process.getWaitingTime();
            case 4:
                return process.getTurnaroundTime();
            default:
                return null;
        }
    }
}
