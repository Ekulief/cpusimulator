import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.*;

public class Processor implements Runnable {
    private int cpuId;
    private int timeQuantum = MainFrame.timeQuantum;
    private JPanel simulatorPanel;
    private RealTimeAnimationManager RRtoCPU;
    private FCFSAnimationManager FCFStoCPU;
    private BatchAnimationManager SJFtoCPU;
    private InteractiveAnimationManager NPPStoCPU;
    private volatile boolean isBusy = false;
    public ReentrantLock lock = new ReentrantLock();
    private static final int MAX_SHIFT_X = 230;
    private volatile boolean running = true;
    private ArrayList<Process> cpu1 = new ArrayList<>();
    private ArrayList<Process> cpu2 = new ArrayList<>();
    private ArrayList<Process> cpu3 = new ArrayList<>();
    private ArrayList<Process> cpu4 = new ArrayList<>();


    public Processor(int cpuId, JPanel panel) {
        this.cpuId = cpuId;
        this.simulatorPanel = panel;
    }

    @Override
    public void run() {
        while (running && CPUSchedulingSimulator.simulationRunning &&
                CPUSchedulingSimulator.completedProcesses.get() < CPUSchedulingSimulator.processes.size()) {
            synchronized (CPUSchedulingSimulator.isPaused) {
                while (CPUSchedulingSimulator.isPaused.get()) {
                    try {
                        CPUSchedulingSimulator.isPaused.wait();
                    } catch (InterruptedException e) {
                        System.out.println("Thread interrupted while sleeping: " + e.getMessage());
                        return; // Exit the thread
                    }
                }
            }

            boolean processExecuted = false;
            processExecuted = executeProcessWithAffinity(QueueManager.mainQueue);

            if (!processExecuted) {
                try {
                    System.out.println("CPU " + cpuId + " is idle at time " + SystemTimer.getCurrentTime());
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Thread interrupted while waiting: " + e.getMessage());
                    return; // Exit the thread
                }
            }
        }
    }

    private boolean executeProcessWithAffinity(ConcurrentLinkedQueue<Process> queue) {
        Process process = queue.poll();

        if (process != null) {
            Processor preferredProcessor = CPUSchedulingSimulator.processorsList.get(process.preferredCPU() - 1);


            if (!preferredProcessor.isBusy && preferredProcessor.lock.tryLock()) {
                try {
                    if (!preferredProcessor.isBusy) {
                        preferredProcessor.isBusy = true;
                        // shiftQueue(queue, process.classification);
                        return preferredProcessor.executeProcess(process);
                    }
                } finally {
                    preferredProcessor.lock.unlock();
                    preferredProcessor.isBusy = false;
                }
            }


            for (Processor otherProcessor : CPUSchedulingSimulator.processorsList) {
                if (otherProcessor.cpuId != preferredProcessor.cpuId && !otherProcessor.isBusy) {
                    if (otherProcessor.lock.tryLock()) {
                        try {
                            if (!otherProcessor.isBusy) {
                                otherProcessor.isBusy = true;
                                // shiftQueue(queue, process.classification);
                                return otherProcessor.executeProcess(process);
                            }
                        } finally {
                            otherProcessor.lock.unlock();
                            otherProcessor.isBusy = false;
                        }
                    }
                }
            }


            synchronized (queue) {
                queue.add(process);
            }
        }
        return false;
    }

    private boolean executeProcess(Process process) {
        synchronized (process) {
            SwingUtilities.invokeLater(() -> {
                JLabel animationLabel = getAnimationLabel(process.pid);
                if (animationLabel != null) {
                    removeAnimationLabel(animationLabel);
                }
            });

            int executionTime = process.classification.equals("Realtime") ? Math.min(timeQuantum, process.remainingTime) : process.remainingTime;

            SwingUtilities.invokeLater(() -> addAnimation(process));

            while (executionTime > 0 && process.remainingTime > 0) {
                synchronized (CPUSchedulingSimulator.isPaused) {
                    while (CPUSchedulingSimulator.isPaused.get()) {
                        try {
                            CPUSchedulingSimulator.isPaused.wait();
                        } catch (InterruptedException e) {
                            return false;
                        }
                    }
                }
                this.isBusy = true;
                process.remainingTime--;
                executionTime--;
                process.turnaroundTime = SystemTimer.getCurrentTime() - process.arrivalTime;
                process.waitingTime = Math.max(0, process.turnaroundTime - process.burstTime);
                process.setStatus("Executing");
                updateGlobalProcessList(process);
                System.out.println("CPU " + cpuId + " processing " + process.pid + " (" + process.classification + ") at time " + SystemTimer.getCurrentTime());

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    return false; // Exit the method if interrupted
                }
            }

            if (process.remainingTime == 0) {
                process.setStatus("Terminated");
                updateGlobalProcessList(process);
                isBusy = false;

                CPUSchedulingSimulator.completeProcess(process);
                SwingUtilities.invokeLater(() -> {
                    addGanttChartPanel(process);
                    proceedToTermination(process);
                });

                System.out.println("CPU " + cpuId + " completed process " + process.pid + " (" + process.classification + ") at time " + SystemTimer.getCurrentTime());
            } else if (process.classification.equals("Realtime")) {
                if (!QueueManager.mainQueue.contains(process)) {
                    process.setStatus("Waiting");
                    QueueManager.mainQueue.add(process);


                    SwingUtilities.invokeLater(() -> {
                        proceedToTermination(process);
                        addGanttChartPanel(process);
                        if (QueueManager.mainQueue.contains(process)) {
                            CPUSchedulingSimulator.addAnimationManager(process, "RR");

                        }


                    });
                }

                System.out.println("CPU " + cpuId + " executed process " + process.pid + " for " + timeQuantum + " units (" + process.classification + ") at time " + SystemTimer.getCurrentTime());
            }
            return true;
        }
    }

    private void addGanttChartPanel(Process process) {
        switch (cpuId) {
            case 1:
                cpu1.add(process);
                MainFrame.chartCPU1.removeAll();
                MainFrame.chartCPU1.add(new GanttChartPanel(cpu1));
                MainFrame.chartCPU1.revalidate();
                MainFrame.chartCPU1.repaint();
                break;
            case 2:
                cpu2.add(process);
                MainFrame.chartCPU2.removeAll();
                MainFrame.chartCPU2.add(new GanttChartPanel(cpu2));
                MainFrame.chartCPU2.revalidate();
                MainFrame.chartCPU2.repaint();
                break;
            case 3:
                cpu3.add(process);
                MainFrame.chartCPU3.removeAll();
                MainFrame.chartCPU3.add(new GanttChartPanel(cpu3));
                MainFrame.chartCPU3.revalidate();
                MainFrame.chartCPU3.repaint();
                break;
            case 4:
                cpu4.add(process);
                MainFrame.chartCPU4.removeAll();
                MainFrame.chartCPU4.add(new GanttChartPanel(cpu4));
                MainFrame.chartCPU4.revalidate();
                MainFrame.chartCPU4.repaint();
                break;
            default:
                throw new IllegalArgumentException("Invalid CPU ID");
        }
    }

    private void proceedToTermination(Process process) {
        switch (process.classification) {
            case "Realtime":
                RRtoCPU.proceedToTermination();
                break;
            case "System":
                FCFStoCPU.proceedToTermination();
                break;
            case "Interactive":
                NPPStoCPU.proceedToTermination();
                break;
            case "Batch":
                SJFtoCPU.proceedToTermination();
                break;
        }
    }

    private void addAnimation(Process process) {

        switch (process.classification) {
            case "Realtime":
                RRtoCPU = new RealTimeAnimationManager(230, process.pid, cpuId);
                simulatorPanel.add(RRtoCPU);
                RRtoCPU.proceedToCPU();
                if(CPUSchedulingSimulator.RRxlocation < 230){
                CPUSchedulingSimulator.RRxlocation += 30;}
                shiftQueue(QueueManager.mainQueue);



                break;
            case "System":
                FCFStoCPU = new FCFSAnimationManager(230, process.pid, cpuId);
                simulatorPanel.add(FCFStoCPU);
                FCFStoCPU.proceedToCPU();
                if(CPUSchedulingSimulator.FCFSxlocation < 230){
                CPUSchedulingSimulator.FCFSxlocation += 30;}
                shiftQueue(QueueManager.mainQueue);



                break;
            case "Interactive":
                NPPStoCPU = new InteractiveAnimationManager(230, process.pid, cpuId);
                simulatorPanel.add(NPPStoCPU);
                NPPStoCPU.proceedToCPU();
                if(CPUSchedulingSimulator.NPPSxlocation < 230) {
                    CPUSchedulingSimulator.NPPSxlocation += 30;
                }
                shiftQueue(QueueManager.mainQueue);



                break;
            case "Batch":
                SJFtoCPU = new BatchAnimationManager(230, process.pid, cpuId);
                simulatorPanel.add(SJFtoCPU);
                SJFtoCPU.proceedToCPU();
                if(CPUSchedulingSimulator.SJFxlocation < 230){
                CPUSchedulingSimulator.SJFxlocation += 30;}
                shiftQueue(QueueManager.mainQueue);


                break;
        }
    }

    private JLabel getAnimationLabel(int pid) {
        synchronized (simulatorPanel) {
            for (Component comp : simulatorPanel.getComponents()) {
                if (comp instanceof JLabel && ((JLabel) comp).getText().equals("P" + pid)) {
                    return (JLabel) comp;
                }
            }
        }
        return null;
    }

    private void removeAnimationLabel(JLabel label) {
        if (label != null) {
            synchronized (simulatorPanel) {
                simulatorPanel.remove(label);
                simulatorPanel.repaint();
            }
        }
    }

    private void updateGlobalProcessList(Process process) {
        CPUSchedulingSimulator.updateProcess(process);
    }

    private void shiftQueue(ConcurrentLinkedQueue<Process> queue) {
        synchronized (simulatorPanel) {
            for (Process p : queue) {
                JLabel label = getAnimationLabel(p.pid);
                if (label != null) {
                    Point location = label.getLocation();
                    if (location.x < MAX_SHIFT_X) {
                        location.x += 30; // Shift right by the defined amount
                        label.setLocation(location);
                    }
                }
            }
            simulatorPanel.repaint();
        }
    }

    public void stopProcessing() {
        running = false;
    }
}

