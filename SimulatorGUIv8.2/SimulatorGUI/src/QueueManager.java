import java.util.concurrent.ConcurrentLinkedQueue;

public class QueueManager {
    public static ConcurrentLinkedQueue<Process> rrQueue = new ConcurrentLinkedQueue<>();
    public static ConcurrentLinkedQueue<Process> fcfsQueue = new ConcurrentLinkedQueue<>();
    public static ConcurrentLinkedQueue<Process> sjfQueue = new ConcurrentLinkedQueue<>();
    public static ConcurrentLinkedQueue<Process> nppsQueue = new ConcurrentLinkedQueue<>();

    public static ConcurrentLinkedQueue<Process> mainQueue = new ConcurrentLinkedQueue<>();
}
