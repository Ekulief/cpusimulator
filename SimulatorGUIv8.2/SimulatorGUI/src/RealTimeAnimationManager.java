import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

class RealTimeAnimationManager extends JLabel {
    private int assignedCPU;
    private int initialX = 230;
    private int initialY = 370;
    Font font = new Font("SansSerif", Font.BOLD, 10);


    public RealTimeAnimationManager(int xlocation, int pID) {

        setBackground(new java.awt.Color(138,255,156));
        setOpaque(true);
        setText("P" + pID);
        setSize(25 , 20);
        setLocation(xlocation, initialY);


    }
    public RealTimeAnimationManager(int xlocation, int pID, int CPU) {

        setBackground(new java.awt.Color(138,255,156));
        setOpaque(true);
        setText("P" + pID);
        setSize(25 , 20);
        setLocation(xlocation, initialY);
        this.assignedCPU = CPU;
        setVerticalAlignment(SwingConstants.CENTER);
        setFont(font);
    }





    public void proceedToCPU() {
        Thread animation = new Thread(new Runnable() {
            @Override
            public void run() {

                int x = 230;
                int y = 365;
                if(assignedCPU == 1){
                if (x < 350) {
                    while (x <= 350) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 305) {
                    while (y <= 505) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 490) {
                    while (x <= 410) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 410) {
                    while (y >= 380) {
                        setLocation(x, y);
                        y -= 1;
                        sleep(1);
                    }
                }
                if (y <= 380) {
                    while (x <= 542) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                }

                if(assignedCPU == 2){
                if (x < 350) {
                    while (x <= 350) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 305) {
                    while (y <= 505) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 490) {
                    while (x <= 410) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 410) {
                    while (y >= 462) {
                        setLocation(x, y);
                        y -= 1;
                        sleep(1);
                    }
                }
                if (y <= 462) {
                    while (x <= 542) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                }
                if(assignedCPU == 3){
                if (x < 350) {
                    while (x <= 350) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 305) {
                    while (y <= 505) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 490) {
                    while (x <= 410) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 410) {
                    while (y <= 561) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 561) {
                    while (x <= 542) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                }
                if(assignedCPU == 4){
                if (x < 350) {
                    while (x <= 350) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 305) {
                    while (y <= 505) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 490) {
                    while (x <= 410) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 410) {
                    while (y <=660) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 660) {
                    while (x <= 542) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                }









            }
        });
        animation.start();
    }


















     public void proceedToTermination() {
        Thread animation = new Thread(new Runnable() {
            @Override
            public void run() {



                if(assignedCPU == 1){
                     int x = 542;
                int y = 375;
                if (x < 620) {
                    while (x <= 620) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 600) {
                    while (y <= 510) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 510) {
                    while (x <= 710) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }


                try {
             Thread.sleep(300);
                        setVisible(false);
                    MainFrame.simulatorPanel.remove(RealTimeAnimationManager.this);

                } catch (InterruptedException ex) {
                    Logger.getLogger(RealTimeAnimationManager.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }

            }

            if(assignedCPU == 2){
                 int x = 542;
                int y = 462;
                if (x < 620) {
                    while (x <= 620) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 600) {
                    while (y <= 510) {
                        setLocation(x, y);
                        y += 1;
                        sleep(1);
                    }
                }
                if (y >= 510) {
                    while (x <= 710) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }


                try {
             Thread.sleep(300);
                        setVisible(false);
                    MainFrame.simulatorPanel.remove(RealTimeAnimationManager.this);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RealTimeAnimationManager.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }

            }


            if(assignedCPU == 3){
                 int x = 542;
                int y = 561;
                if (x < 620) {
                    while (x <= 620) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 600) {
                    while (y >= 510) {
                        setLocation(x, y);
                        y -= 1;
                        sleep(1);
                    }
                }
                if (y <= 510) {
                    while (x <= 710) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }


                try {
             Thread.sleep(300);
                        setVisible(false);
                    MainFrame.simulatorPanel.remove(RealTimeAnimationManager.this);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RealTimeAnimationManager.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }

            }



            if(assignedCPU == 4){
                 int x = 542;
                int y = 660;
                if (x < 620) {
                    while (x <= 620) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }
                if (x >= 600) {
                    while (y >= 510) {
                        setLocation(x, y);
                        y -= 1;
                        sleep(1);
                    }
                }
                if (y <= 510) {
                    while (x <= 710) {
                        setLocation(x, y);
                        x += 1;
                        sleep(1);
                    }
                }


                try {
             Thread.sleep(300);
                        setVisible(false);
                    MainFrame.simulatorPanel.remove(RealTimeAnimationManager.this);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RealTimeAnimationManager.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }

            }





             }
        });
        animation.start();
    }

















    private void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            System.out.println("Thread interrupted while waiting: " + e.getMessage());
            Thread.currentThread().interrupt(); // Set interrupted status
            return; // Exit from the method
        }
    }


}
