public class SystemTimer {
    private static int currentTime = 0;

    public static synchronized void incrementTime(int increment) {
        currentTime += increment;
        System.out.println("System Timer incremented to: " + currentTime);
    }

    public static synchronized int getCurrentTime() {
        return currentTime;
    }

    public static synchronized void reset() {
        currentTime = 0;
        System.out.println("System Timer reset to: " + currentTime);
    }
}
